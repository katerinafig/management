
if (document.getElementById("gen1") != null)
    document.getElementById("gen1").setAttribute("checked", "checked");
$(document).ready(function () {
    $("#gen1, #gen2").change(function () {
        if ($("#gen1").is(":checked")) {
            $("#woman_image").show();
            $("#man_image").hide();
        } else if ($("#gen2").is(":checked")) {
            $("#woman_image").hide();
            $("#man_image").show();
        }
    });
});

if(document.getElementById("loginPlayer") != null){
    var _Seconds = $('.seconds').text(),
        int;
    var isSubmit=false;
    $("#submitNewStage").mousedown( function () {
        isSubmit=true;
    });
    int = setInterval(function () { // запускаем интервал
        if(!isSubmit) {
            if (_Seconds > 0) {
                _Seconds--; // вычитаем 1
                $('.seconds').text(_Seconds); // выводим получившееся значение в блок
            } else {
                clearInterval(int); // очищаем интервал, чтобы он не продолжал работу при _Seconds = 0
                alert('Автоматический переход.');
                xhr = new XMLHttpRequest();
                xhr.open('POST', "http://localhost:8082/management/newStage/" + $("#loginPlayer").text());
                xhr.onload = function () {
                    console.log(xhr.responseText);
                    document.location.href = xhr.responseURL;
                    document.location.reload();
                };
                xhr.send();
            }
        }
    }, 1000);
}

$("#ESM1StageCount").change(function () {
    $("#ESM1StageMoney").attr('max', $("#capital").val() / $("#ESM1StageCount").val());

});
$("#build_avtomat_fabr")
    .change(function () {
        $("#avtomat_fabrica_cost").text($(this).val()*10000);
        $("#build_obch_fabr").attr("max",(($("#capital").val()- $("#auth_obch_fabr").val()*7000-$(this).val()*10000)/5000)| 0);
        $("#auth_obch_fabr").attr("max", (($("#capital").val()- $("#build_obch_fabr").val()*5000-$(this).val()*10000)/7000)| 0);
    })
    .change();
$("#build_obch_fabr")
    .change(function () {
        $("#obch_fabrica_cost").text($(this).val()*5000+ $("#auth_obch_fabr").val()*7000);
        $("#build_avtomat_fabr").attr("max",(($("#capital").val()- $("#auth_obch_fabr").val()*7000-$(this).val()*5000)/10000)| 0);
        $("#auth_obch_fabr").attr("max", (($("#capital").val()- $("#build_avtomat_fabr").val()*10000-$(this).val()*5000)/7000)| 0);


    })
    .change();
$("#automatLoan")
    .change(function () {
        $("#mexanLoan").attr("max",(($("#capital").val()/2- $("#mexanLoan").val()*5000-$("#automatLoan").val()*10000)/10000)| 0);
    })
    .change();
$("#mexanLoan")
    .change(function () {
        $("#automatLoan").attr("max",(($("#capital").val()/2- $("#mexanLoan").val()*5000-$("#automatLoan").val()*10000)/5000)| 0);
    })
    .change();
$("#auth_obch_fabr")
    .change(function () {
        $("#obch_fabrica_cost").text($(this).val()*7000+$("#build_obch_fabr").val()*5000);
        $("#build_obch_fabr").attr("max",(($("#capital").val()- $("#build_avtomat_fabr").val()*10000-$(this).val()*7000)/5000)| 0);
        $("#build_avtomat_fabr").attr("max",(($("#capital").val()- $("#build_obch_fabr").val()*5000-$(this).val()*7000)/10000)| 0);
    })
    .change();
if ($("#capital").val() <= 0) {
    alert("Ты банкрот, сученька;)");
    xhr = new XMLHttpRequest();
    xhr.open('POST', "http://localhost:8080/management/exit/" + $("#loginPlayer").text());
    xhr.onload = function () {
        console.log(xhr.responseText);
        document.location.href = xhr.responseURL;
    };
    xhr.send();
}

$("#mexanESM")
    .change(function () {
        var number = $("#mexanESM").val();
        $("#sumMoneyObESM").text(number * 2000 + "");
    })
    .change();
$("#avtomatESM")
    .change(function () {
        var number = $("#avtomatESM").val();
        var partNumber = number/2;
        if(number%2===0){
            $("#sumMoneyAvtESM").text(partNumber*3000+"");
        }
        else{
            $("#sumMoneyAvtESM").text(partNumber*3000+2000+"");
        }

    })
    .change();

var modal1 = document.getElementById('myModal1');
var btn1 = document.getElementById("myBtn1");

var modal2 = document.getElementById('myModal2');
var btn2 = document.getElementById("myBtn2");

var modal3 = document.getElementById('myModal3');
var btn3 = document.getElementById("myBtn3");

var modal4 = document.getElementById('myModal4');
var btn4 = document.getElementById("myBtn4");

var modal5 = document.getElementById('myModal5');
var btn5 = document.getElementById("myBtn5");

var cancel= document.getElementById("cancelBtn");

var span = document.getElementsByClassName("close")[0];
var myModal = document.getElementById('myModal');

if(btn1!=null)
btn1.onclick = function() {
    modal1.style.display = "block";
};

if(btn2!=null)
btn2.onclick = function() {
    modal2.style.display = "block";
};
if(btn3!=null)
btn3.onclick = function() {
    modal3.style.display = "block";
};
if(btn4!=null)
btn4.onclick = function() {
    modal4.style.display = "block";
};
if(btn5!=null)
    btn5.onclick = function() {
        modal5.style.display = "block";
    };
span.onclick = function () {
    if (modal1 != null) {
        modal1.style.display = "none";
    }
    if (modal2 != null) {
        modal2.style.display = "none";
    }
    if (modal3 != null) {
        modal3.style.display = "none";
    }
    if (modal4 != null) {
        modal4.style.display = "none";
    }
    if (modal5 != null) {
        modal5.style.display = "none";
    }
};

cancel.onclick=function(){
    if (modal1 != null) {
        modal1.style.display = "none";
    }
    if (modal2 != null) {
        modal2.style.display = "none";
    }
    if (modal3 != null) {
        modal3.style.display = "none";
    }
    if (modal4 != null) {
        modal4.style.display = "none";
    }
    if (myModal != null) {
        myModal.style.display = "none";
    }
    if (modal5 != null) {
        modal5.style.display = "none";
    }
};

// window.onclick = function(event) {
//     if (event.target == modal) {
//         modal.style.display = "none";
//     }
// }

// var _Seconds = $('.seconds').text(),
//     int;
// int = setInterval(function() { // запускаем интервал
//     if (_Seconds > 0) {
//         _Seconds--; // вычитаем 1
//         $('.seconds').text(_Seconds); // выводим получившееся значение в блок
//     } else {
//         clearInterval(int); // очищаем интервал, чтобы он не продолжал работу при _Seconds = 0
//         alert('End!');
//     }
// }, 1000);