package com.ssau.gruppa6414.controllers;

import com.ssau.gruppa6414.models.Player;
import com.ssau.gruppa6414.models.Room;
import com.ssau.gruppa6414.services.EntryService;
import com.ssau.gruppa6414.services.model.PlayerDataService;
import com.ssau.gruppa6414.services.model.RoomDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@RestController
@RequestMapping("/server")
public class ServerController {
    @Autowired
    private PlayerDataService playerDataService;
    @Autowired
    private RoomDataService roomDataService;
    @Autowired
    private EntryService entryService;
    @PostMapping(value = "/newPlayer", consumes = "application/json")
    public Player postNewPlayer(@RequestBody Player player, HttpServletResponse response) {
        return  playerDataService.addPlayer(player);
    }
    @PostMapping(value = "/newRoom/{id}", consumes = "application/json")
    public Room postNewRoom(@PathVariable(name = "id")String id, @RequestBody Room room, HttpServletResponse response) {
        return entryService.createRoom(id,room);
    }
    @GetMapping(value = "/entryInRoom/{login}/{room}")
    public Room entryInRoom(@PathVariable(name = "login")String login,@PathVariable(name = "room")String room, HttpServletResponse response) {
        return entryService.entryPlayerInRoom(login,
                Integer.parseInt(room));
    }
    @GetMapping(value = "/getRooms")
    public Collection<Room> getAvailableRooms(){
        return roomDataService.getListAvailableRooms();
    }

    @GetMapping(value = "/getPlayer/{login}")
    public Player getPlayer(@PathVariable(name = "login")String login){
        return playerDataService.getPlayer(login);
    }
    @GetMapping(value = "/getRoom/{id}")
    public Room getRoom(@PathVariable(name = "id")String id){
        return roomDataService.getRoom(Integer.parseInt(id));
    }
    @GetMapping(value = "/exit/{login}")
    public Player entryInRoom(@PathVariable(name = "login")String login,HttpServletResponse response) {
        return entryService.exitPlayerFromRoom(login);
    }
    @GetMapping(value = "/getOtherPlayerInRoom/{login}")
    public Collection<Player> getOtherPlayerInRoom(@PathVariable(name = "login")String login,HttpServletResponse response) {
        Player player = playerDataService.getPlayer(login);
        Collection<Player> otherPlayers = player.getRoom().getPlayers();
        otherPlayers.remove(player);
        return otherPlayers;
    }
    @GetMapping(value = "/newStage/{login}")
    public Player newStage(@PathVariable(name = "login")String login,HttpServletResponse response) {
        Player player = playerDataService.getPlayer(login);

        return player;
    }
}

