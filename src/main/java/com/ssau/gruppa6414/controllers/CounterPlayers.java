package com.ssau.gruppa6414.controllers;

public class CounterPlayers {
    private static CounterPlayers instance;
    public Integer value;

    private CounterPlayers(Integer value) {
        this.value = value;
    }

    public static CounterPlayers getInstance(Integer value) {
        if (instance == null) {
            instance = new CounterPlayers(value);
        }
        return instance;
    }
    public Integer deletePlayer(){
        return value--;
    }
    public Integer addPlayer(){
        return value++;
    }
}
