package com.ssau.gruppa6414.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssau.gruppa6414.models.*;
import com.ssau.gruppa6414.repositories.BidRepository;
import com.ssau.gruppa6414.repositories.GameStepRepository;
import com.ssau.gruppa6414.repositories.LoanRepository;
import com.ssau.gruppa6414.services.BankService;
import com.ssau.gruppa6414.services.EntryService;
import com.ssau.gruppa6414.services.GameService;
import com.ssau.gruppa6414.services.model.*;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;


@Controller
@RequestMapping("/management")
public class ClientController {
    @Autowired
    PlayerDataService playerDataService;
    @Autowired
    BankService bankService;
    @Autowired
    GameService gameService;
    @Autowired
    RoomDataService roomDataService;
    @Autowired
    GameStepDataService gameStepDataService;
    @Autowired
    GameStepRepository gameStepRepository;
    @Autowired
    BidRepository bidRepository;
    @Autowired
    BidDataService bidDataService;
    @Autowired
    LoanDataService loanDataService;
    @Autowired
    LoanRepository loanRepository;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    EntryService entryService;
    private CounterPlayers counterPlayers;
    @GetMapping("/auth")
    public String getConnectedGame(@RequestParam(name = "login", required = false)String login,Model model) {
        Player player;
        if(login==null) {
            player = new Player();
        }
        else{
             player = restTemplate.getForObject("http://localhost:8082/server/getPlayer/"+login,Player.class);
             restTemplate.getForObject("http://localhost:8082/server/getPlayer/", JSONObject.class);
        }
        Room room = new Room();
        model.addAttribute("player", player);
        model.addAttribute("room", room);
        return "registration";
    }
    @PostMapping("/creating")
    public String postCreatingGame(Model model,@ModelAttribute(name = "player") Player player) {
        Room room = new Room();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Player> entity = new HttpEntity<>(player,headers);
        Player newPlayer= restTemplate.postForObject("http://localhost:8082/server/newPlayer",
                entity, Player.class);
        if(newPlayer!=null) {
            model.addAttribute("player", newPlayer);
            model.addAttribute("room", room);
            return "creategame";
        }
        else{
            Player oldPlayer = restTemplate.getForObject("http://localhost:8082/server/getPlayer/"+player.getLogin(),Player.class);
            Player [] otherPlayers = restTemplate.getForObject("http://localhost:8082/server/getOtherPlayerInRoom/" + player.getLogin(), Player[].class);
            model.addAttribute("player", oldPlayer);
            model.addAttribute("step", gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(oldPlayer));
            model.addAttribute("otherPlayers", otherPlayers);
            model.addAttribute("room", oldPlayer.getRoom());
            model.addAttribute("table", false);
            model.addAttribute("repeat", false);
            return "field";
        }
    }
    @PostMapping("/creating/{login}")
    public String postCreatingAndSave(@PathVariable(name = "login")String login,
                                      @ModelAttribute(name = "room") Room room, Model model){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Room> entityRoom = new HttpEntity<>(room,headers);
        Room newRoom = restTemplate.postForObject("http://localhost:8082/server/newRoom/"+login,
                entityRoom, Room.class);

        model.addAttribute("room", newRoom);
        model.addAttribute("login", login);
        return "redirect:/management/room?login="+login;
    }
    @PostMapping("/exit/{login}")
    public String postExit(@PathVariable(name = "login")String login, Model model){
        Player newPlayer = restTemplate.getForObject("http://localhost:8082/server/exit/"+login, Player.class);
        model.addAttribute("room", newPlayer);
        model.addAttribute("idPlayer", login);
        return "redirect:/management/auth?login="+login;
    }

    @GetMapping("/room")
    public String postCreatingAndSave(@RequestParam(name = "login") String login, @RequestParam(name = "id", required = false) String idRoom,
                                      @RequestParam(name = "table", required = false) String table,@RequestParam(name=  "repeat", required = false)String repeat, Model model) {
        Player player = restTemplate.getForObject("http://localhost:8082/server/getPlayer/" + login, Player.class);
        if (player != null) {
            Room room = null;
            Player[] otherPlayers = null;
            Bid bid = new Bid();
            Loan loan = new Loan();
            FactoryBuilding factoryBuilding = new FactoryBuilding();
            if(repeat!=null){
                if("true".equals(repeat)){
                    model.addAttribute("repeat", true);
                }
                else{
                    model.addAttribute("repeat", false);
                }
            }
            else{
                model.addAttribute("repeat", false);
            }
            if (player.getRoom() != null) {
                room = restTemplate.getForObject("http://localhost:8082/server/entryInRoom/" + login + "/" + player.getRoom().getRoomId(), Room.class);
                otherPlayers = restTemplate.getForObject("http://localhost:8082/server/getOtherPlayerInRoom/" + login, Player[].class);
                /*int j=0;
                while (!gameStepRepository.getLastGameStep(room.getRoomId()).getStepNumber().equals(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStepNumber())) {
                    j=1;
                }*/

                if(repeat==null||!repeat.equals("true")) {
                    if (table != null) {
                        int i = 0;
                        while (gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStageNumber() != 2) {
                            if (gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStageNumber() == 4)
                                break;
                            i = 1;
                        }
                        model.addAttribute("lastSteps", gameStepRepository.getGameStepsByRoomAndStageNumberAndStepNumber(room
                                , gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStageNumber() - 1, gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStepNumber()));

                        if (table.equals("y")) {
                            model.addAttribute("table", true);
                            model.addAttribute("repeat", false);
                        } else {
                            model.addAttribute("table", false);
                        }
                    } else {
                        model.addAttribute("lastSteps", null);
                        model.addAttribute("table", false);
                    }
                }
                else {
                    model.addAttribute("lastSteps", null);
                    model.addAttribute("table", false);
                }

                model.addAttribute("room", room);
                model.addAttribute("player", player);
                model.addAttribute("bid", bid);
                model.addAttribute("loan", loan);
                model.addAttribute("factoryBuilding", factoryBuilding);
                model.addAttribute("step", gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player));
                model.addAttribute("otherPlayers", otherPlayers);
                return "field";
            } else if (idRoom != null) {
                room = restTemplate.getForObject("http://localhost:8082/server/entryInRoom/" + login + "/" + idRoom, Room.class);
                otherPlayers = restTemplate.getForObject("http://localhost:8082/server/getOtherPlayerInRoom/" + login, Player[].class);
               /*int j=0;
                while (!gameStepRepository.getLastGameStep(room.getRoomId()).getStepNumber().equals(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStepNumber())) {
                    j=1;
                }*/
                if(repeat==null||!repeat.equals("true")) {
                    if (table != null) {
                        int i = 0;
                        while (gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStageNumber() != 2) {
                            if (gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStageNumber() == 4)
                                break;
                            i = 1;
                        }
                        model.addAttribute("lastSteps", gameStepRepository.getGameStepsByRoomAndStageNumberAndStepNumber(room
                                , gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStageNumber() - 1, gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player).getStepNumber()));

                        if (table.equals("y")) {
                            model.addAttribute("table", true);
                            model.addAttribute("repeat", false);
                        } else {
                            model.addAttribute("table", false);
                        }
                    } else {
                        model.addAttribute("lastSteps", null);
                        model.addAttribute("table", false);
                    }
                }
                else {
                    model.addAttribute("lastSteps", null);
                    model.addAttribute("table", false);
                }

                model.addAttribute("room", room);
                model.addAttribute("player", player);
                model.addAttribute("bid", bid);
                model.addAttribute("loan", loan);
                model.addAttribute("factoryBuilding", factoryBuilding);
                model.addAttribute("step", gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player));
                model.addAttribute("otherPlayers", otherPlayers);
                return "field";
            }
        }
        return "redirect:/management/connection?login=" + login;
    }

    @PostMapping("/connection")
    public String postConnectedGame(@ModelAttribute(name = "player") Player player, Model model) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Player> entity = new HttpEntity<>(player,headers);
        Player newPlayer= restTemplate.postForObject("http://localhost:8082/server/newPlayer",
                entity, Player.class);
        if(newPlayer!=null) {
            ResponseEntity<Room[]>entityRooms =
                    restTemplate.getForEntity("http://localhost:8082/server/getRooms",Room[].class);
            Room[] rooms = entityRooms.getBody();

            if(rooms==null||rooms.length==0) {
                return "redirect:/management/auth?login=" + newPlayer.getLogin();
            }
            model.addAttribute("newPlayer", newPlayer);
            model.addAttribute("rooms",rooms);
            return "podkluch";
        }
        else{
            Player oldPlayer = restTemplate.getForObject("http://localhost:8082/server/getPlayer/"+player.getLogin(),Player.class);
            model.addAttribute("player", oldPlayer);
            model.addAttribute("room", oldPlayer.getRoom());
            return "redirect:/management/room?login="+oldPlayer.getLogin();
        }
    }
    @PostMapping("/newStage/{login}")
    public String postNewStage(@PathVariable(name = "login")String login, Model model){
        Player player = playerDataService.getPlayer(login);
        GameStep step = gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player);
        boolean waiting;
        waiting=false;
        if (player.getSeniorityNumber() == step.getStepNumber() % step.getRoom().getCurrentNumberOfPlayers() + 1) {
            player.setMadeMove(false);
        }
        else{
            player.setMadeMove(true);

        }
        playerDataService.updatePlayer(player);
        Player []otherPlayers = restTemplate.getForObject("http://localhost:8082/server/getOtherPlayerInRoom/" + login, Player[].class);

        for (Player curPlayer : otherPlayers) {
            if (!curPlayer.getMadeMove()) {
                waiting = true;
                break;
            }
        }
        if(!waiting) {

            if (step.getStageNumber() == 1) {
                if (step.getBid() == null) {
                    Bid bid = new Bid();
                    bid.setAmountOfMoney(0);
                    bid.setNumberOfUnits(0);
                    bid.setBankDecision(0);
                    bidDataService.saveBid(bid);
                    step.setBid(bid);
                    gameStepRepository.save(step);
                }
                if (player.getSeniorityNumber() == step.getStepNumber() % step.getRoom().getCurrentNumberOfPlayers() + 1) {
                    gameStepDataService.goNewStageBeforeESM(roomDataService
                            .getRoom(player.getRoom().getRoomId()));
                    player.setMadeMove(true);
                    playerDataService.updatePlayer(player);
                } else {
                    step = gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player);
                    model.addAttribute("step", step);
                }
                model.addAttribute("room", player);
                model.addAttribute("idPlayer", login);
                return "redirect:/management/room?login=" + login + "&table=y";
            } else if (step.getStageNumber() == 2) {
                if (player.getSeniorityNumber() == step.getStepNumber() % step.getRoom().getCurrentNumberOfPlayers() + 1) {
                    gameStepDataService.goNewStageBeforeProcess(roomDataService
                            .getRoom(player.getRoom().getRoomId()));
                    player.setMadeMove(true);
                    playerDataService.updatePlayer(player);
                } else {
                    step = gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player);
                    model.addAttribute("step", step);
                }
                model.addAttribute("room", player);
                model.addAttribute("idPlayer", login);
                return "redirect:/management/room?login=" + login;
            } else if (step.getStageNumber() == 3) {
                if (step.getBid() == null) {
                    Bid bid = new Bid();
                    bid.setAmountOfMoney(0);
                    bid.setNumberOfUnits(0);
                    bid.setBankDecision(0);
                    bidDataService.saveBid(bid);
                    step.setBid(bid);
                    gameStepRepository.save(step);
                }
                if (player.getSeniorityNumber() == step.getStepNumber() % step.getRoom().getCurrentNumberOfPlayers() + 1) {
                    gameStepDataService.goNewStageBeforeEGP(roomDataService
                            .getRoom(player.getRoom().getRoomId()));
                    player.setMadeMove(true);
                    playerDataService.updatePlayer(player);
                } else {
                    step = gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player);
                    model.addAttribute("step", step);
                }
                model.addAttribute("room", player);
                model.addAttribute("idPlayer", login);
                return "redirect:/management/room?login=" + login + "&table=y";
            } else if (step.getStageNumber() == 4) {
                if (player.getSeniorityNumber() == step.getStepNumber() % step.getRoom().getCurrentNumberOfPlayers() + 1) {
                    gameStepDataService.goNewStep(roomDataService
                            .getRoom(player.getRoom().getRoomId()));
                    player.setMadeMove(true);
                    playerDataService.updatePlayer(player);
                } else {
                    step = gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player);
                    model.addAttribute("step", step);
                }
                model.addAttribute("room", player);
                model.addAttribute("idPlayer", login);
                return "redirect:/management/room?login=" + login;
            }
            else {
                return "redirect:/management/room?login=" + login;
            }
        }
            else {
            return "redirect:/management/room?login=" + login+"&repeat=true";
        }
    }
    @PostMapping("/applyESM/{login}")
    public String postApplyESM(@PathVariable(name = "login")String login, @ModelAttribute Bid bid, Model model){
        Player player = playerDataService.getPlayer(login);
        if(bid==null||bid.getAmountOfMoney()==null||bid.getNumberOfUnits()==null){
            Bid newBid = new Bid();
            newBid.setAmountOfMoney(0);
            newBid.setNumberOfUnits(0);
            newBid.setBankDecision(0);
            gameService.firstESMStage(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player),newBid);
        }
        else{
            gameService.firstESMStage(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player),bid);
        }
        model.addAttribute("room", player.getRoom());
        model.addAttribute("player", player);
        return "redirect:/management/room?login="+login;
    }
    @PostMapping("/applyProcess/{login}")
    public String postProcessESM(@PathVariable(name = "login")String login, @ModelAttribute FactoryBuilding factoryBuilding, Model model){
        Player player = playerDataService.getPlayer(login);
        if(factoryBuilding==null){
            FactoryBuilding newFactoryBuilding = new FactoryBuilding();
            newFactoryBuilding.setNumberOfBasicFactory(0);
            newFactoryBuilding.setNumberOfAutoFactory(0);
            gameService.secondProcessStage(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player),newFactoryBuilding.getNumberOfBasicFactory(),newFactoryBuilding.getNumberOfAutoFactory());
        }
        else{
            gameService.secondProcessStage(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player),factoryBuilding.getNumberOfBasicFactory(), factoryBuilding.getNumberOfAutoFactory());
        }
        model.addAttribute("room", player.getRoom());
        model.addAttribute("player", player);
        return "redirect:/management/room?login="+login;
    }
    @PostMapping("/applyEGP/{login}")
    public String postApplyEGP(@PathVariable(name = "login")String login, @ModelAttribute Bid bid, Model model){
        Player player = playerDataService.getPlayer(login);
        if(bid==null||bid.getAmountOfMoney()==null||bid.getNumberOfUnits()==null){
            Bid newBid = new Bid();
            newBid.setAmountOfMoney(0);
            newBid.setNumberOfUnits(0);
            newBid.setBankDecision(0);
            gameService.thirdEGPStage(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player),newBid);
        }
        else{
            gameService.thirdEGPStage(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player),bid);
        }
        model.addAttribute("room", player.getRoom());
        model.addAttribute("player", player);
        return "redirect:/management/room?login="+login;
    }
    @PostMapping("/applyLoans/{login}")
    public String postLoans(@PathVariable(name = "login")String login, @ModelAttribute Loan loan, Model model){
        Player player = playerDataService.getPlayer(login);
        if(loan!=null){
            if(loan.getNumberLoanForAutoPlants()==null){
                loan.setNumberLoanForAutoPlants(0);
            }
            if(loan.getNumberLoanForBasicPlants()==null){
                loan.setNumberLoanForBasicPlants(0);
            }
            gameService.fourLoanStage(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player),loan);
        }
        model.addAttribute("room", player.getRoom());
        model.addAttribute("player", player);
        return "redirect:/management/room?login="+login;
    }
    @PostMapping("/applyBuildings/{login}")
    public String postBuildings(@PathVariable(name = "login")String login, @ModelAttribute FactoryBuilding factoryBuilding, Model model){
        Player player = playerDataService.getPlayer(login);
        if(factoryBuilding!=null){
            if(factoryBuilding.getNumberOfBasicFactory()==null){
                factoryBuilding.setNumberOfBasicFactory(0);
            }
            if(factoryBuilding.getNumberOfAutoFactory()==null){
                factoryBuilding.setNumberOfAutoFactory(0);
            }
            if(factoryBuilding.getNumberOfProcessFactory()==null){
                factoryBuilding.setNumberOfProcessFactory(0);
            }
            gameService.fourBuildingStage(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc(player),factoryBuilding);
        }
        model.addAttribute("room", player.getRoom());
        model.addAttribute("player", player);
        return "redirect:/management/room?login="+login;
    }
    @GetMapping("/question/{login}")
    public String getQuestion(@PathVariable(name = "login")String login, Model model){
        model.addAttribute("login",login);
        return "spravka";
    }
    @GetMapping("/history/{login}")
    public String getHostory(@PathVariable(name = "login")String login, Model model){
        Player player=playerDataService.getPlayer(login);
        model.addAttribute("steps",gameStepRepository
                .getGameStepsByPlayer(player));
        model.addAttribute("buildings",player.getFactoryBuildings());
        model.addAttribute("loans",player.getLoans());
        model.addAttribute("login",login);
        return "History";
    }

}
