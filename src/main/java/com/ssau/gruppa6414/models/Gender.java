package com.ssau.gruppa6414.models;

public enum Gender {
    MAN("Мужской",2,false),
    WOMAN("Женский",1, true);
    private String text;
    private Integer number;
    private Boolean isWoman;
    Gender() {
    }

    Gender(String text, Integer number, Boolean isWoman) {
        this.text = text;
        this.number=number;
        this.isWoman=isWoman;
    }
    public String getText() {
        return this.text;
    }
    public Integer getNumber(){ return this.number;}
    public Boolean getWoman() {
        return isWoman;
    }

    public static Gender[] getValues(){
        Gender[] result = new Gender[2];
        result[0]=Gender.MAN;
        result[1]=Gender.WOMAN;
        return result;
    }
}
