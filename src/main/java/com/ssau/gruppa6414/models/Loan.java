package com.ssau.gruppa6414.models;


import javax.persistence.*;

@Entity
@Table(name = "Loans")
public class Loan {
    @Id
    @GeneratedValue
    @Column(name = "loanId")
    private Integer loanId;
    @Column(name = "numberLoanForBasicPlants")
    private Integer numberLoanForBasicPlants;
    @Column(name = "numberLoanForAutoPlants")
    private Integer numberLoanForAutoPlants;
    @Column(name = "startStep")
    private Integer startStep;
    @ManyToOne(targetEntity = Player.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "playerId")
    private Player player;

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getNumberLoanForBasicPlants() {
        return numberLoanForBasicPlants;
    }

    public void setNumberLoanForBasicPlants(Integer numberLoanForBasicPlants) {
        this.numberLoanForBasicPlants = numberLoanForBasicPlants;
    }

    public Integer getNumberLoanForAutoPlants() {
        return numberLoanForAutoPlants;
    }

    public void setNumberLoanForAutoPlants(Integer numberLoanForAutoPlants) {
        this.numberLoanForAutoPlants = numberLoanForAutoPlants;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Integer getStartStep() {
        return startStep;
    }

    public void setStartStep(Integer startStep) {
        this.startStep = startStep;
    }

}
