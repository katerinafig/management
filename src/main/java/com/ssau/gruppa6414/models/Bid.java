package com.ssau.gruppa6414.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Bids")
public class Bid {
    @Id
    @GeneratedValue
    @Column(name = "bidId")
    private Integer bidId;
    @Column(name = "typeBid")
    private TypeBid typeBid;
    @Column(name = "amountOfMoney")
    private Integer amountOfMoney;
    @Column(name = "numberOfUnits")
    private Integer numberOfUnits;
    @Column(name = "bankDecision")
    private Integer bankDecision;
    @OneToMany(mappedBy="bid", cascade = CascadeType.ALL)
    private Set<GameStep> gameSteps;

    public Integer getBidId() {
        return bidId;
    }

    public void setBidId(Integer bidId) {
        this.bidId = bidId;
    }

    public TypeBid getTypeBid() {
        return typeBid;
    }

    public void setTypeBid(TypeBid typeBid) {
        this.typeBid = typeBid;
    }

    public Integer getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(Integer amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Integer getBankDecision() {
        return bankDecision;
    }

    public void setBankDecision(Integer bankDecision) {
        this.bankDecision = bankDecision;
    }

    public Set<GameStep> getGameSteps() {
        return gameSteps;
    }

    public void setGameSteps(Set<GameStep> gameSteps) {
        this.gameSteps = gameSteps;
    }
}
