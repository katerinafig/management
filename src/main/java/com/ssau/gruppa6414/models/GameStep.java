package com.ssau.gruppa6414.models;

import javax.persistence.*;

@Entity
@Table(name = "GameSteps")
public class GameStep {
    @Id
    @GeneratedValue
    @Column(name = "gameStepId")
    private Integer gameStepId;
    @Column(name = "stepNumber")
    private Integer stepNumber;
    @Column(name = "stageNumber")
    private Integer stageNumber;
    @ManyToOne(targetEntity = Room.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "roomId")
    private Room room;
    @ManyToOne(targetEntity = Player.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "playerId")
    private Player player;
    @Column(name="level")
    private Integer level;
    @Column(name = "capital")
    private Integer capital;
    @Column(name = "numberOfESM")
    private Integer numberOfESM;
    @Column(name = "numberOfEGP")
    private Integer numberOfEGP;
    @Column(name = "numberOfBasicPlants")
    private Integer numberOfBasicPlants;
    @Column(name = "numberOfAutoPlants")
    private Integer numberOfAutoPlants;
    @ManyToOne(targetEntity = Bid.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "bidId")
    private Bid bid;

    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public Integer getStageNumber() {
        return stageNumber;
    }

    public void setStageNumber(Integer stageNumber) {
        this.stageNumber = stageNumber;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Integer getCapital() {
        return capital;
    }

    public void setCapital(Integer capital) {
        this.capital = capital;
    }

    public Integer getNumberOfESM() {
        return numberOfESM;
    }

    public void setNumberOfESM(Integer numberOfESM) {
        this.numberOfESM = numberOfESM;
    }

    public Integer getNumberOfEGP() {
        return numberOfEGP;
    }

    public void setNumberOfEGP(Integer numberOfEGP) {
        this.numberOfEGP = numberOfEGP;
    }

    public Integer getNumberOfBasicPlants() {
        return numberOfBasicPlants;
    }

    public void setNumberOfBasicPlants(Integer numberOfBasicPlants) {
        this.numberOfBasicPlants = numberOfBasicPlants;
    }

    public Integer getNumberOfAutoPlants() {
        return numberOfAutoPlants;
    }

    public void setNumberOfAutoPlants(Integer numberOfAutoPlants) {
        this.numberOfAutoPlants = numberOfAutoPlants;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Bid getBid() {
        return bid;
    }

    public void setBid(Bid bid) {
        this.bid = bid;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Integer getGameStepId() {
        return gameStepId;
    }

    public void setGameStepId(Integer gameStepId) {
        this.gameStepId = gameStepId;
    }

    public int getCountESM() {
        int countPlayer = getRoom().getCurrentNumberOfPlayers();
        switch (getLevel()) {
            case 1:
                return countPlayer;
            case 2:
                return (int) (countPlayer * 1.5);
            case 3: return countPlayer*2;
            case 4: return (int)(countPlayer*2.5);
            case 5: return countPlayer*3;
            default: return 0;
        }
    }
    public int getMinMoneyESM(){
        switch (getLevel()){
            case 1: return 800;
            case 2: return 650;
            case 3: return 500;
            case 4: return 400;
            case 5: return 300;
            default: return 0;
        }
    }
    public int getCountEGP(){
        int countPlayer = getRoom().getCurrentNumberOfPlayers();
        switch (getLevel()){
            case 1: return countPlayer*3;
            case 2: return (int)(countPlayer*2.5);
            case 3: return countPlayer*2;
            case 4: return (int)(countPlayer*1.5);
            case 5: return countPlayer;
            default: return 0;
        }
    }
    public int getMaxMoneyEGP(){
        switch (getLevel()){
            case 1: return 6500;
            case 2: return 6000;
            case 3: return 5500;
            case 4: return 5000;
            case 5: return 4500;
            default: return 0;
        }
    }
}
