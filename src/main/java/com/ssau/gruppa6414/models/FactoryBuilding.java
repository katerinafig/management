package com.ssau.gruppa6414.models;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "FactoryBuildings")
public class FactoryBuilding {
    @Id
    @GeneratedValue
    @Column(name = "buildingId")
    private Integer buildingId;
    @Column(name = "numberOfBasicFactory")
    private Integer numberOfBasicFactory;
    @Column(name = "numberOfAutoFactory")
    private Integer numberOfAutoFactory;
    @Column(name = "numberOfProcessFactory")
    private Integer numberOfProcessFactory;
    @ManyToOne(targetEntity = Player.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "playerId")
    private Player player;
    @Column(name = "startStep")
    private Integer startStep;
    public Integer getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Integer buildingId) {
        this.buildingId = buildingId;
    }

    public Integer getNumberOfBasicFactory() {
        return numberOfBasicFactory;
    }

    public void setNumberOfBasicFactory(Integer numberOfBasicFactory) {
        this.numberOfBasicFactory = numberOfBasicFactory;
    }

    public Integer getNumberOfAutoFactory() {
        return numberOfAutoFactory;
    }

    public void setNumberOfAutoFactory(Integer numberOfAutoFactory) {
        this.numberOfAutoFactory = numberOfAutoFactory;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Integer getStartStep() {
        return startStep;
    }

    public void setStartStep(Integer startStep) {
        this.startStep = startStep;
    }

    public Integer getNumberOfProcessFactory() {
        return numberOfProcessFactory;
    }

    public void setNumberOfProcessFactory(Integer numberOfProcessFactory) {
        this.numberOfProcessFactory = numberOfProcessFactory;
    }
}
