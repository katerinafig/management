package com.ssau.gruppa6414.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ssau.gruppa6414.deserialization.RoomDeserializer;
import com.ssau.gruppa6414.serialization.RoomSerializer;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
@JsonSerialize(using = RoomSerializer.class)
@JsonDeserialize(using = RoomDeserializer.class)
@Entity
@Table(name = "Rooms")
public class Room {
    @Id
    @GeneratedValue
    @Column(name = "roomId")
    private Integer roomId;
    @Column(name = "name")
    private String name;
    @Column(name = "numberOfPlayers")
    private Integer numberOfPlayers;
    @Column(name = "currentNumberOfPlayers")
    private Integer currentNumberOfPlayers;
    @OneToMany(mappedBy="room", cascade = CascadeType.ALL)
    private Set<GameStep> gameSteps;
    @OneToMany(mappedBy="room", cascade = CascadeType.ALL)
    private List<Player> players;

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<GameStep> getGameSteps() {
        return gameSteps;
    }

    public void setGameSteps(Set<GameStep> gameSteps) {
        this.gameSteps = gameSteps;
    }

    public Integer getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(Integer numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public Integer getCurrentNumberOfPlayers() {
        return currentNumberOfPlayers;
    }

    public void setCurrentNumberOfPlayers(Integer currentNumberOfPlayers) {
        this.currentNumberOfPlayers = currentNumberOfPlayers;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

}
