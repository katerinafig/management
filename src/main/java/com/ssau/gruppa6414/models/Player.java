package com.ssau.gruppa6414.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ssau.gruppa6414.deserialization.PlayerDeserializer;
import com.ssau.gruppa6414.serialization.PlayerSerializer;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.persistence.*;
import java.util.Set;
@JsonSerialize(using = PlayerSerializer.class)
@JsonDeserialize(using = PlayerDeserializer.class)
@Entity
@SessionAttributes
@Table(name = "Players")
public class Player {
    @Id
    @Column(name = "login")
    private String login;
    @Column(name = "gender")
    private Gender gender;
    @Column(name = "isMadeMove")
    private Boolean isMadeMove;
    @Column(name = "avatar")
    private String avatar;
    @Column(name = "seniorityNumber")
    private Integer seniorityNumber;
    @ManyToOne(targetEntity = Room.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "idRoom")
    private Room room;
    @OneToMany(mappedBy="player", cascade = CascadeType.ALL)
    private Set<GameStep> gameSteps;
    @OneToMany(mappedBy="player", cascade = CascadeType.ALL)
    private Set<Loan> loans;
    @OneToMany(mappedBy="player", cascade = CascadeType.ALL)
    private Set<FactoryBuilding> factoryBuildings;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Set<GameStep> getGameSteps() {
        return gameSteps;
    }

    public void setGameSteps(Set<GameStep> gameSteps) {
        this.gameSteps = gameSteps;
    }

    public Set<Loan> getLoans() {
        return loans;
    }

    public void setLoans(Set<Loan> loans) {
        this.loans = loans;
    }

    public Set<FactoryBuilding> getFactoryBuildings() {
        return factoryBuildings;
    }

    public void setFactoryBuildings(Set<FactoryBuilding> factoryBuildings) {
        this.factoryBuildings = factoryBuildings;
    }

    public Integer getSeniorityNumber() {
        return seniorityNumber;
    }

    public void setSeniorityNumber(Integer seniorityNumber) {
        this.seniorityNumber = seniorityNumber;
    }

    public Boolean getMadeMove() {
        return isMadeMove;
    }

    public void setMadeMove(Boolean madeMove) {
        isMadeMove = madeMove;
    }
}
