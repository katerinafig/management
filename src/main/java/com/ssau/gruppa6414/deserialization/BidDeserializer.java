package com.ssau.gruppa6414.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ssau.gruppa6414.models.Bid;
import com.ssau.gruppa6414.models.TypeBid;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class BidDeserializer extends JsonDeserializer<Bid> {
    @Override
    public Bid deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Bid result = new Bid();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode bidId = (((ObjectNode) treeNode).findValue("bidId"));
        if (bidId != null)
            result.setBidId(bidId.asInt());
        JsonNode typeBid = (((ObjectNode) treeNode).findValue("typeBid"));
        if (typeBid != null)
            result.setTypeBid(TypeBid.valueOf(typeBid.asText()));
        JsonNode amountOfMoney = (((ObjectNode) treeNode).findValue("amountOfMoney"));
        if (amountOfMoney != null)
            result.setAmountOfMoney(amountOfMoney.asInt());
        JsonNode numberOfUnits = (((ObjectNode) treeNode).findValue("numberOfUnits"));
        if (numberOfUnits != null)
            result.setNumberOfUnits(numberOfUnits.asInt());
        JsonNode bankDecision = (((ObjectNode) treeNode).findValue("bankDecision"));
        if (bankDecision != null)
            result.setBankDecision(bankDecision.asInt());

        return result;
    }
}
