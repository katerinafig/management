package com.ssau.gruppa6414.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ssau.gruppa6414.models.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
@JsonComponent
public class RoomDeserializer extends JsonDeserializer<Room> {
    @Autowired
   private ObjectMapper objectMapper;
    @Override
    public Room deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Room result = new Room();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode roomId = (((ObjectNode) treeNode).findValue("roomId"));
        if (roomId != null)
            result.setRoomId(roomId.asInt());
        JsonNode name = (((ObjectNode) treeNode).findValue("name"));
        if (name != null)
            result.setName(name.asText());
        JsonNode numberOfPlayers = (((ObjectNode) treeNode).findValue("numberOfPlayers"));
        if (numberOfPlayers != null)
            result.setNumberOfPlayers(numberOfPlayers.asInt());
        JsonNode currentNumberOfPlayers = (((ObjectNode) treeNode).findValue("currentNumberOfPlayers"));
        if (currentNumberOfPlayers != null)
            result.setCurrentNumberOfPlayers(currentNumberOfPlayers.asInt());

        return result;
    }
}
