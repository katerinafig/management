package com.ssau.gruppa6414.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ssau.gruppa6414.models.Gender;
import com.ssau.gruppa6414.models.Player;
import com.ssau.gruppa6414.models.Room;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
@JsonComponent
public class PlayerDeserializer extends JsonDeserializer<Player> {

    @Override
    public Player deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        Player result = new Player();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode login = (((ObjectNode) treeNode).findValue("login"));
        if (login != null)
            result.setLogin(login.asText());
        JsonNode room = (((ObjectNode) treeNode).findValue("room"));
        if (room != null) {
            result.setRoom(objectMapper.readValue(room.toString(),Room.class));
        }
        JsonNode seniorityNumber = (((ObjectNode) treeNode).findValue("seniorityNumber"));
        if (seniorityNumber != null)
            result.setSeniorityNumber(seniorityNumber.asInt());
        JsonNode isMadeMove = (((ObjectNode) treeNode).findValue("isMadeMove"));
        if (isMadeMove != null)
            result.setMadeMove(isMadeMove.asBoolean());
        JsonNode gender = (((ObjectNode) treeNode).findValue("gender"));
        if (gender != null)
            result.setGender(Gender.valueOf(gender.asText()));
        JsonNode avatar = (((ObjectNode) treeNode).findValue("avatar"));
        if (avatar != null)
            result.setAvatar(avatar.asText());

         return result;
    }
}
