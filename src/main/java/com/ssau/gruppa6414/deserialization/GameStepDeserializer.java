package com.ssau.gruppa6414.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ssau.gruppa6414.models.*;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class GameStepDeserializer extends JsonDeserializer<GameStep> {
    @Override
    public GameStep deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        GameStep result = new GameStep();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode gameStepId = (((ObjectNode) treeNode).findValue("gameStepId"));
        if (gameStepId != null)
            result.setGameStepId(gameStepId.asInt());
        JsonNode stepNumber = (((ObjectNode) treeNode).findValue("stepNumber"));
        if (stepNumber != null)
            result.setStepNumber(stepNumber.asInt());
        JsonNode stageNumber = (((ObjectNode) treeNode).findValue("stageNumber"));
        if (stageNumber != null)
            result.setStageNumber(stageNumber.asInt());
        JsonNode room = (((ObjectNode) treeNode).findValue("room"));
        if (room != null)
            result.setRoom(objectMapper.readValue(room.toString(), Room.class));
        JsonNode player = (((ObjectNode) treeNode).findValue("player"));
        if (player != null)
            result.setPlayer(objectMapper.readValue(player.toString(), Player.class));
        JsonNode level = (((ObjectNode) treeNode).findValue("level"));
        if (level != null)
            result.setLevel(level.asInt());
        JsonNode capital = (((ObjectNode) treeNode).findValue("capital"));
        if (capital != null)
            result.setCapital(capital.asInt());
        JsonNode numberOfESM = (((ObjectNode) treeNode).findValue("numberOfESM"));
        if (numberOfESM != null)
            result.setNumberOfESM(numberOfESM.asInt());
        JsonNode numberOfEGP = (((ObjectNode) treeNode).findValue("numberOfEGP"));
        if (numberOfEGP != null)
            result.setNumberOfEGP(numberOfEGP.asInt());
        JsonNode numberOfBasicPlants = (((ObjectNode) treeNode).findValue("numberOfBasicPlants"));
        if (numberOfBasicPlants != null)
            result.setNumberOfBasicPlants(numberOfBasicPlants.asInt());
        JsonNode numberOfAutoPlants = (((ObjectNode) treeNode).findValue("numberOfAutoPlants"));
        if (numberOfAutoPlants != null)
            result.setNumberOfAutoPlants(numberOfAutoPlants.asInt());
        JsonNode bid = (((ObjectNode) treeNode).findValue("bid"));
        if (bid != null) {
            result.setBid(objectMapper.readValue(bid.toString(), Bid.class));
        }
        return result;
    }
}
