package com.ssau.gruppa6414.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ssau.gruppa6414.models.Loan;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class LoanDeserializer extends JsonDeserializer<Loan> {
    @Override
    public Loan deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Loan result = new Loan();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode loanId = (((ObjectNode) treeNode).findValue("loanId"));
        if (loanId != null)
            result.setLoanId(loanId.asInt());
        JsonNode numberLoanForBasicPlants = (((ObjectNode) treeNode).findValue("numberLoanForBasicPlants"));
        if (numberLoanForBasicPlants != null)
            result.setNumberLoanForBasicPlants(numberLoanForBasicPlants.asInt());
        JsonNode numberLoanForAutoPlants = (((ObjectNode) treeNode).findValue("numberLoanForAutoPlants"));
        if (numberLoanForAutoPlants != null)
            result.setNumberLoanForAutoPlants(numberLoanForAutoPlants.asInt());
        JsonNode startStep = (((ObjectNode) treeNode).findValue("startStep"));
        if (startStep != null)
            result.setStartStep(startStep.asInt());
        return result;
    }
}
