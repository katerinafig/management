package com.ssau.gruppa6414.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ssau.gruppa6414.models.FactoryBuilding;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class FactoryBuildingDeserializer extends JsonDeserializer<FactoryBuilding> {
    @Override
    public FactoryBuilding deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        FactoryBuilding result = new FactoryBuilding();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode buildingId = (((ObjectNode) treeNode).findValue("buildingId"));
        if (buildingId != null)
            result.setBuildingId(buildingId.asInt());
        JsonNode numberOfBasicFactory = (((ObjectNode) treeNode).findValue("numberOfBasicFactory"));
        if (numberOfBasicFactory != null)
            result.setNumberOfBasicFactory(numberOfBasicFactory.asInt());
        JsonNode numberOfProcessFactory = (((ObjectNode) treeNode).findValue("numberOfProcessFactory"));
        if (numberOfProcessFactory != null)
            result.setNumberOfProcessFactory(numberOfProcessFactory.asInt());
        JsonNode numberOfAutoFactory = (((ObjectNode) treeNode).findValue("numberOfAutoFactory"));
        if (numberOfAutoFactory != null)
            result.setNumberOfAutoFactory(numberOfAutoFactory.asInt());
        JsonNode startStep = (((ObjectNode) treeNode).findValue("startStep"));
        if (startStep != null)
            result.setStartStep(startStep.asInt());
        return result;
    }
}
