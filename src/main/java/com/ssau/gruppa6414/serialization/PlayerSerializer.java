package com.ssau.gruppa6414.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ssau.gruppa6414.models.Player;

import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
@JsonComponent
public class PlayerSerializer extends JsonSerializer<Player> {
    @Override
    public void serialize(Player value, JsonGenerator jgen, SerializerProvider serializers)
            throws IOException {
        jgen.writeStartObject();
        jgen.writeStringField("login", value.getLogin());
        if(value.getRoom()!=null) {
            jgen.writeObjectField("room",value.getRoom());
        }
        if(value.getSeniorityNumber()!=null) {
            jgen.writeNumberField("seniorityNumber",value.getSeniorityNumber());
        }
        if(value.getMadeMove()!=null) {
            jgen.writeBooleanField("isMadeMove",value.getMadeMove());
        }
        if(value.getGender()!=null)
            jgen.writeStringField("gender", value.getGender().toString());
        if(value.getAvatar()!=null)
            jgen.writeStringField("avatar",
                    value.getAvatar());
        jgen.writeEndObject();
    }
}
