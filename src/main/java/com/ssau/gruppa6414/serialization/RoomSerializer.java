package com.ssau.gruppa6414.serialization;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ssau.gruppa6414.models.Player;
import com.ssau.gruppa6414.models.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
@JsonComponent
public class RoomSerializer extends JsonSerializer<Room> {
    @Override
    public void serialize(Room value, JsonGenerator jgen, SerializerProvider serializers)
            throws IOException {
        jgen.writeStartObject();
        if(value.getRoomId()!=null)
        jgen.writeNumberField("roomId", value.getRoomId());
        if(value.getName()!=null)
            jgen.writeStringField("name", value.getName());
        if(value.getNumberOfPlayers()!=null)
            jgen.writeNumberField("numberOfPlayers", value.getNumberOfPlayers());
        if(value.getCurrentNumberOfPlayers()!=null)
            jgen.writeNumberField("currentNumberOfPlayers",
                    value.getCurrentNumberOfPlayers());
        jgen.writeEndObject();
    }
}
