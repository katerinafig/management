package com.ssau.gruppa6414.serialization;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ssau.gruppa6414.models.FactoryBuilding;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
@JsonComponent
public class FactoryBuildingSerializer extends JsonSerializer<FactoryBuilding> {
    @Override
    public void serialize(FactoryBuilding value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {
        jgen.writeStartObject();
        jgen.writeNumberField("buildingId", value.getBuildingId());
        if(value.getNumberOfBasicFactory()!=null) {
            jgen.writeNumberField("numberOfBasicFactory",value.getNumberOfBasicFactory());
        }
        if(value.getNumberOfAutoFactory()!=null) {
            jgen.writeNumberField("numberOfAutoFactory",value.getNumberOfAutoFactory());
        }
        if(value.getNumberOfProcessFactory()!=null) {
            jgen.writeNumberField("numberOfProcessFactory",value.getNumberOfProcessFactory());
        }
        if(value.getStartStep()!=null) {
            jgen.writeNumberField("startStep",value.getStartStep());
        }
        jgen.writeEndObject();
    }
}
