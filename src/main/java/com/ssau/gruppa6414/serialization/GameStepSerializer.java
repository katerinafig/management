package com.ssau.gruppa6414.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ssau.gruppa6414.models.GameStep;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class GameStepSerializer extends JsonSerializer<GameStep> {
    @Override
    public void serialize(GameStep value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {
        jgen.writeStartObject();

        jgen.writeNumberField("gameStepId", value.getGameStepId());
        if(value.getStepNumber()!=null) {
            jgen.writeNumberField("stepNumber",value.getStepNumber());
        }
        if(value.getStageNumber()!=null) {
            jgen.writeNumberField("stageNumber",value.getStageNumber());
        }
        if(value.getRoom()!=null) {
            jgen.writeObjectField("room", value.getRoom());
        }
        if(value.getPlayer()!=null) {
            jgen.writeObjectField("player",value.getPlayer());
        }
        if(value.getLevel()!=null) {
            jgen.writeNumberField("level",value.getLevel());
        }
        if(value.getCapital()!=null) {
            jgen.writeNumberField("capital",value.getCapital());
        }
        if(value.getNumberOfESM()!=null) {
            jgen.writeNumberField("numberOfESM",value.getNumberOfESM());
        }
        if(value.getNumberOfEGP()!=null) {
            jgen.writeNumberField("numberOfEGP",value.getNumberOfEGP());
        }
        if(value.getNumberOfBasicPlants()!=null) {
            jgen.writeNumberField("numberOfBasicPlants",value.getNumberOfBasicPlants());
        }
        if(value.getNumberOfAutoPlants()!=null) {
            jgen.writeNumberField("numberOfAutoPlants",value.getNumberOfAutoPlants());
        }
        if(value.getBid()!=null) {
            jgen.writeObjectField("bid",value.getBid());
        }
        jgen.writeEndObject();
    }
}
