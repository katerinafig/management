package com.ssau.gruppa6414.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ssau.gruppa6414.models.Loan;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
@JsonComponent
public class LoanSerializer extends JsonSerializer<Loan> {
    @Override
    public void serialize(Loan value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {
        jgen.writeStartObject();
        jgen.writeNumberField("loanId", value.getLoanId());
        if(value.getNumberLoanForBasicPlants()!=null) {
            jgen.writeNumberField("numberLoanForBasicPlants",value.getNumberLoanForBasicPlants());
        }
        if(value.getNumberLoanForAutoPlants()!=null) {
            jgen.writeNumberField("numberLoanForAutoPlants",value.getNumberLoanForAutoPlants());
        }
        if(value.getStartStep()!=null) {
            jgen.writeNumberField("startStep",value.getStartStep());
        }
        jgen.writeEndObject();
    }
}
