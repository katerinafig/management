package com.ssau.gruppa6414.serialization;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ssau.gruppa6414.models.Bid;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
@JsonComponent
public class BidSerializer extends JsonSerializer<Bid> {
    @Override
    public void serialize(Bid value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {
        jgen.writeStartObject();
        jgen.writeNumberField("bidId", value.getBidId());
        if(value.getTypeBid()!=null) {
            jgen.writeStringField("typeBid",value.getTypeBid().toString());
        }
        if(value.getAmountOfMoney()!=null) {
            jgen.writeNumberField("amountOfMoney",value.getAmountOfMoney());
        }
        if(value.getNumberOfUnits()!=null) {
            jgen.writeNumberField("numberOfUnits",value.getNumberOfUnits());
        }
        if(value.getBankDecision()!=null) {
            jgen.writeNumberField("bankDecision",value.getBankDecision());
        }
        jgen.writeEndObject();
    }
}
