package com.ssau.gruppa6414.services.model;

import com.ssau.gruppa6414.models.Player;
import com.ssau.gruppa6414.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerDataService {
    @Autowired
    private PlayerRepository playerRepository;
    public Player addPlayer( Player player){
        if (playerRepository.findById(player.getLogin()).isEmpty()
                ||playerRepository.findById(player.getLogin()).isPresent()
                &&playerRepository.getOne(player.getLogin()).getRoom()==null) {
            return playerRepository.save(player);
        }
        else return null;
    }

    public Player getPlayer(String id) {
        return playerRepository.getOne(id);
    }
    public Player updatePlayer(Player player) {
        return playerRepository.saveAndFlush(player);
    }
}
