package com.ssau.gruppa6414.services.model;

import com.ssau.gruppa6414.models.Bid;
import com.ssau.gruppa6414.repositories.BidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BidDataService {
    @Autowired
    private BidRepository bidRepository;

    public Bid saveBid(Bid bid){
        return bidRepository.saveAndFlush(bid);
    }

}
