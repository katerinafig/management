package com.ssau.gruppa6414.services.model;

import com.ssau.gruppa6414.models.Room;
import com.ssau.gruppa6414.repositories.GameStepRepository;
import com.ssau.gruppa6414.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class RoomDataService {
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private GameStepRepository gameStepRepository;
    public Room createRoom( Room room){
        return roomRepository.save(room);
    }
    public Collection<Room> getListAvailableRooms (){
       ArrayList<Room> rooms = (ArrayList<Room>)roomRepository.getAvailableRooms();
       for(int i=0;i<rooms.size();i++){
           if(gameStepRepository.getLastGameStep(rooms.get(i).getRoomId()).getStepNumber()>1
                   ||gameStepRepository.getLastGameStep(rooms.get(i).getRoomId()).getStageNumber()>1){
               rooms.remove(rooms.get(i));
           }
       }
       return rooms;
    }
    public Room getRoom(Integer id){
        return  roomRepository.getOne(id);
    }

}
