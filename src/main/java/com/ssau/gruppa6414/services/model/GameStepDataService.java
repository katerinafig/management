package com.ssau.gruppa6414.services.model;

import com.ssau.gruppa6414.models.FactoryBuilding;
import com.ssau.gruppa6414.models.GameStep;
import com.ssau.gruppa6414.models.Player;
import com.ssau.gruppa6414.models.Room;
import com.ssau.gruppa6414.repositories.GameStepRepository;
import com.ssau.gruppa6414.services.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class GameStepDataService {
    @Autowired
    BankService bankService;
    @Autowired
    PlayerDataService playerDataService;
    @Autowired
    RoomDataService roomDataService;
    @Autowired
    private GameStepRepository gameStepRepository;
    private final static Double[][] arrayMarkov = {
            {0.34,0.67,0.84,0.92,1.0},
            {0.25,0.58,0.83,0.91,1.0},
            {0.08,0.33,0.67,0.92,1.0},
            {0.08,0.17,0.42,0.75,1.0},
            {0.08,0.17,0.33,0.67,1.0}};

    public Integer changeLevel(GameStep step){
        double x = ThreadLocalRandom.current().nextDouble(0.0, 1.0);
        int newStep=0;
        for (int i =0;i<5;i++){
            if((i==0&&x < arrayMarkov[step.getLevel() - 1][i])||
                    (i!=0&&(x < arrayMarkov[step.getLevel() - 1][i])&&(x>arrayMarkov[step.getLevel() - 1][i-1]))){
                newStep =i+1;
            }
        }
        return newStep;
    }
    public GameStep createFirstStep(Player player){
        GameStep step= new GameStep();
        player.setMadeMove(false);
        player.setSeniorityNumber(player.getRoom().getCurrentNumberOfPlayers());
        step.setPlayer(player);
        step.setRoom(player.getRoom());
        step.setStepNumber(1);
        step.setStageNumber(1);
        step.setNumberOfBasicPlants(2);
        step.setNumberOfAutoPlants(0);
        step.setNumberOfESM(4);
        step.setNumberOfEGP(2);
        step.setCapital(10000);
        step.setLevel(3);
        return gameStepRepository.save(step);
    }

    public Room goNewStep(Room room) {
        int newLevel = changeLevel(gameStepRepository.getTopByPlayerOrderByGameStepIdDesc
                (room.getPlayers().get(0)));
        for (Player player : room.getPlayers()) {
            player.setMadeMove(false);
            playerDataService.updatePlayer(player);
        }
        for(GameStep currentStep:gameStepRepository.getLastStepsFromRoom(room.getRoomId())){
            GameStep newStep = new GameStep();
            newStep.setPlayer(currentStep.getPlayer());
            newStep.setRoom(currentStep.getRoom());
            newStep.setStepNumber(currentStep.getStepNumber()+1);
            newStep.setStageNumber(1);
            newStep.setNumberOfBasicPlants(currentStep.getNumberOfBasicPlants());
            newStep.setNumberOfAutoPlants(currentStep.getNumberOfAutoPlants());
            for(FactoryBuilding factoryBuilding:currentStep.getPlayer().getFactoryBuildings()){
                if(currentStep.getStepNumber() - factoryBuilding.getStartStep()>=5){
                    newStep.setNumberOfBasicPlants(currentStep.getNumberOfBasicPlants()
                            +factoryBuilding.getNumberOfBasicFactory());
                }
                if(currentStep.getStepNumber() - factoryBuilding.getStartStep()>=7){
                    newStep.setNumberOfAutoPlants(currentStep.getNumberOfAutoPlants()
                            +factoryBuilding.getNumberOfAutoFactory());
                }
                if(currentStep.getStepNumber() - factoryBuilding.getStartStep()>=9){
                    newStep.setNumberOfAutoPlants(currentStep.getNumberOfAutoPlants()
                            +factoryBuilding.getNumberOfProcessFactory());
                    newStep.setNumberOfBasicPlants(currentStep.getNumberOfBasicPlants()
                            -factoryBuilding.getNumberOfProcessFactory());
                }
            }

            newStep.setNumberOfESM(currentStep.getNumberOfESM());
            newStep.setNumberOfEGP(currentStep.getNumberOfEGP());
            newStep.setCapital(currentStep.getCapital());
            newStep.setLevel(newLevel);
            bankService.takeMoney(gameStepRepository.save(newStep));
        }
        return room;
    }

    public Room goNewStageBeforeESM(Room room) {

        bankService.decisionBankByESM(gameStepRepository.getLastStepsFromRoom(room.getRoomId()));
        for (Player player : room.getPlayers()) {
            player.setMadeMove(false);
            playerDataService.updatePlayer(player);
        }
        for(GameStep currentStep: gameStepRepository.getLastStepsFromRoom(room.getRoomId())){
            GameStep newStep = new GameStep();
            newStep.setPlayer(currentStep.getPlayer());
            newStep.setRoom(currentStep.getRoom());
            newStep.setStepNumber(currentStep.getStepNumber());
            newStep.setLevel(currentStep.getLevel());
            newStep.setStageNumber(2);
            newStep.setNumberOfBasicPlants(currentStep.getNumberOfBasicPlants());
            newStep.setNumberOfAutoPlants(currentStep.getNumberOfAutoPlants());
            newStep.setNumberOfESM(currentStep.getNumberOfESM());
            newStep.setNumberOfEGP(currentStep.getNumberOfEGP());
            newStep.setCapital(currentStep.getCapital());
            gameStepRepository.save(newStep);
        }
        return room;

    }
    public Room goNewStageBeforeProcess(Room room) {

        for (Player player : room.getPlayers()) {
            player.setMadeMove(false);
            playerDataService.updatePlayer(player);
        }
        for(GameStep currentStep: gameStepRepository.getLastStepsFromRoom(room.getRoomId())){
            GameStep newStep = new GameStep();
            newStep.setPlayer(currentStep.getPlayer());
            newStep.setRoom(currentStep.getRoom());
            newStep.setStepNumber(currentStep.getStepNumber());
            newStep.setLevel(currentStep.getLevel());
            newStep.setStageNumber(3);
            newStep.setNumberOfBasicPlants(currentStep.getNumberOfBasicPlants());
            newStep.setNumberOfAutoPlants(currentStep.getNumberOfAutoPlants());
            newStep.setNumberOfESM(currentStep.getNumberOfESM());
            newStep.setNumberOfEGP(currentStep.getNumberOfEGP());
            newStep.setCapital(currentStep.getCapital());
            gameStepRepository.save(newStep);
        }
        return room;
    }
    public Room goNewStageBeforeEGP(Room room) {

        bankService.decisionBankByEGP(gameStepRepository.getLastStepsFromRoom(room.getRoomId()));
        for (Player player : room.getPlayers()) {
            player.setMadeMove(false);
            playerDataService.updatePlayer(player);
        }
        for(GameStep currentStep: gameStepRepository.getLastStepsFromRoom(room.getRoomId())){
            GameStep newStep = new GameStep();
            newStep.setPlayer(currentStep.getPlayer());
            newStep.setRoom(currentStep.getRoom());
            newStep.setStepNumber(currentStep.getStepNumber());
            newStep.setLevel(currentStep.getLevel());
            newStep.setStageNumber(4);
            newStep.setBid(currentStep.getBid());
            newStep.setNumberOfBasicPlants(currentStep.getNumberOfBasicPlants());
            newStep.setNumberOfAutoPlants(currentStep.getNumberOfAutoPlants());
            newStep.setNumberOfESM(currentStep.getNumberOfESM());
            newStep.setNumberOfEGP(currentStep.getNumberOfEGP());
            newStep.setCapital(currentStep.getCapital());
            gameStepRepository.save(newStep);
        }
        return room;
    }

    public GameStep saveStep(GameStep step){
       return gameStepRepository.save(step);
    }
    public GameStep getStep(Integer stepId){
        return gameStepRepository.getOne(stepId);
    }


}
