package com.ssau.gruppa6414.services.model;

import com.ssau.gruppa6414.models.GameStep;
import com.ssau.gruppa6414.models.Loan;
import com.ssau.gruppa6414.repositories.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class LoanDataService {
    @Autowired
    private LoanRepository loanRepository;
    @Autowired
    private PlayerDataService playerDataService;
    public Loan takeLoanByStep(Loan loan, GameStep step) {
        Collection<Loan> openLoans = loanRepository.getOpenedLoansByPlayerAndCurrentStep(step.getStepNumber(),step.getPlayer().getLogin());
        int sum = 0;
        for (Loan openLoan : openLoans) {
            sum =+ openLoan.getNumberLoanForAutoPlants() * 10000 + openLoan.getNumberLoanForBasicPlants() * 5000;
        }
        loan.setPlayer(playerDataService.getPlayer(step.getPlayer().getLogin()));
        if (sum <= step.getCapital() / 2
                && loan.getNumberLoanForBasicPlants()<=step.getNumberOfBasicPlants()
                && loan.getNumberLoanForAutoPlants()<=step.getNumberOfAutoPlants()) {
            return loanRepository.save(loan);
        } else {
            return null;
        }
    }
    public Collection<Loan> getOpenedLoans(GameStep step){
        return loanRepository.getOpenedLoansByPlayerAndCurrentStep(step.getStepNumber(),step.getPlayer().getLogin());
    }
}
