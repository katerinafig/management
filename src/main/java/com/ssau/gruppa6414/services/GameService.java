package com.ssau.gruppa6414.services;

import com.ssau.gruppa6414.models.*;
import com.ssau.gruppa6414.repositories.FactoryBuildingRepository;
import com.ssau.gruppa6414.services.model.BidDataService;
import com.ssau.gruppa6414.services.model.GameStepDataService;
import com.ssau.gruppa6414.services.model.LoanDataService;
import com.ssau.gruppa6414.services.model.PlayerDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameService {
    @Autowired
    BidDataService bidDataService;
    @Autowired
    GameStepDataService gameStepDataService;
    @Autowired
    PlayerDataService playerDataService;
    @Autowired
    LoanDataService loanDataService;
    @Autowired
    FactoryBuildingRepository factoryBuildingRepository;
    public void applyBid(GameStep step,Bid bid){

        Bid savedBid = bidDataService.saveBid(bid);
        GameStep gameStep = gameStepDataService.getStep(step.getGameStepId());
        gameStep.setBid(savedBid);
        gameStepDataService.saveStep(gameStep);
    }
    public void firstESMStage(GameStep step,Bid bid){
        if(step.getStageNumber()==1) {
            applyBid(step, bid);
        }
    }
    public void secondProcessStage(GameStep step, Integer countESMOnBasic,Integer countEGPOnAuto){
        if(step.getStageNumber()==2) {
            if(countEGPOnAuto==null)countEGPOnAuto=0;
            if(countESMOnBasic==null)countESMOnBasic=0;
            step.setNumberOfEGP(step.getNumberOfEGP()+countESMOnBasic+countEGPOnAuto);
            step.setNumberOfESM(step.getNumberOfESM() - countESMOnBasic-countEGPOnAuto);
            int i = countEGPOnAuto/2;
            step.setCapital(step.getCapital()-2000*(countESMOnBasic+countEGPOnAuto-i*2)-3000*i);
            gameStepDataService.saveStep(step);
            playerDataService.updatePlayer(step.getPlayer());

        }
    }
    public void thirdEGPStage(GameStep step,Bid bid){
        if(step.getStageNumber()==3) {
            playerDataService.updatePlayer(step.getPlayer());
            applyBid(step, bid);
        }
    }
    public void fourBuildingStage(GameStep step,FactoryBuilding factoryBuilding){
        if(step.getStageNumber()==4) {
            Player player = playerDataService.getPlayer(step.getPlayer().getLogin());
            factoryBuilding.setPlayer(player);
            factoryBuilding.setStartStep(step.getStepNumber());
            factoryBuildingRepository.save(factoryBuilding);
            step.setCapital(step.getCapital()
                    - factoryBuilding.getNumberOfProcessFactory()*7000
                    -factoryBuilding.getNumberOfAutoFactory()*10000
                    -factoryBuilding.getNumberOfBasicFactory()*5000);
            gameStepDataService.saveStep(step);
            playerDataService.updatePlayer(player);
        }
    }
    public void fourLoanStage(GameStep step, Loan loan){
        if(step.getStageNumber()==4) {
            Player player = playerDataService.getPlayer(step.getPlayer().getLogin());
            loan.setPlayer(player);
            loan.setStartStep(step.getStageNumber());
            playerDataService.updatePlayer(step.getPlayer());
            step.setCapital(step.getCapital()
                    + loan.getNumberLoanForBasicPlants()*5000
                    +loan.getNumberLoanForAutoPlants()*10000);
            gameStepDataService.saveStep(step);
            loanDataService.takeLoanByStep(loan,step);
        }
    }

}
