package com.ssau.gruppa6414.services;

import com.ssau.gruppa6414.controllers.ServerController;
import com.ssau.gruppa6414.models.GameStep;
import com.ssau.gruppa6414.models.Player;
import com.ssau.gruppa6414.models.Room;
import com.ssau.gruppa6414.repositories.GameStepRepository;
import com.ssau.gruppa6414.repositories.PlayerRepository;
import com.ssau.gruppa6414.repositories.RoomRepository;
import com.ssau.gruppa6414.services.model.GameStepDataService;
import com.ssau.gruppa6414.services.model.PlayerDataService;
import com.ssau.gruppa6414.services.model.RoomDataService;
import org.springframework.batch.core.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class EntryService {
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private RoomDataService roomDataService;
    @Autowired
    private GameStepDataService gameStepDataService;
    @Autowired
    private BankService bankService;
    @Autowired
    private GameStepRepository gameStepRepository;
    @Autowired
    private PlayerDataService playerDataService;

    public Room createRoom(String login, Room room){
        Player currentPlayer = playerRepository.getOne(login);
        if(currentPlayer.getRoom()==null){
            Room currentRoom = roomDataService.createRoom(room);
            currentRoom.setCurrentNumberOfPlayers(1);
            currentPlayer.setRoom(currentRoom);
            gameStepDataService.createFirstStep(currentPlayer);
            playerRepository.save(currentPlayer);
            return roomRepository.save(currentRoom);
        }
        return null;
    }
    public Room entryPlayerInRoom(String login, Integer roomId){
        Player currentPlayer = playerRepository.getOne(login);
        Room currentRoom = roomRepository.getOne(roomId);
        if(currentRoom.getCurrentNumberOfPlayers()<currentRoom.getNumberOfPlayers()
                &&currentPlayer.getRoom()==null) {
            currentPlayer.setRoom(currentRoom);
            currentRoom.setCurrentNumberOfPlayers(currentRoom.getCurrentNumberOfPlayers()+1);
            gameStepDataService.createFirstStep(currentPlayer);
            playerRepository.save(currentPlayer);
        }
        return roomRepository.save(currentRoom);
    }
    public Player exitPlayerFromRoom(String login){
        Player currentPlayer = playerDataService.getPlayer(login);
        if(currentPlayer.getRoom()!=null) {
            currentPlayer.getRoom().setCurrentNumberOfPlayers(
                    currentPlayer.getRoom().getCurrentNumberOfPlayers() - 1);
            Collection<Player> otherPlayers = currentPlayer.getRoom().getPlayers();
            otherPlayers.remove(currentPlayer);
            int i = 1;
            for (Player player : otherPlayers) {
                player.setSeniorityNumber(i);
                playerDataService.updatePlayer(player);
                i++;
            }
            Room room = roomDataService.getRoom(currentPlayer.getRoom().getRoomId());
            Collection<GameStep> gameSteps = new ArrayList<>();
            for (GameStep gameStep : currentPlayer.getGameSteps()) {
                gameSteps.add(gameStepRepository.getOne(gameStep.getGameStepId()));
            }
            currentPlayer.setRoom(null);
            currentPlayer.setGameSteps(null);
            playerRepository.save(currentPlayer);
            if (room.getCurrentNumberOfPlayers() > 0) {
                for (GameStep gameStep : gameSteps) {
                    gameStepRepository.delete(gameStep);
                }
            } else {
                for (GameStep gameStep : gameSteps) {
                    gameStepRepository.delete(gameStep);
                }
                roomRepository.delete(room);
            }

        }
        return playerRepository.save(currentPlayer);
    }

}
