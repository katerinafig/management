package com.ssau.gruppa6414.services;

import com.ssau.gruppa6414.models.Bid;
import com.ssau.gruppa6414.models.GameStep;
import com.ssau.gruppa6414.models.Loan;
import com.ssau.gruppa6414.models.Player;
import com.ssau.gruppa6414.repositories.GameStepRepository;
import com.ssau.gruppa6414.services.model.BidDataService;
import com.ssau.gruppa6414.services.model.LoanDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class BankService {
    @Autowired
    private LoanDataService loanDataService;
    @Autowired
    private BidDataService bidDataService;
    @Autowired
    private GameStepRepository gameStepRepository;
    public void takeMoney(GameStep step){
        Collection<Loan> openLoans = loanDataService.getOpenedLoans(step);
        double sum = 0;
        for (Loan openLoan : openLoans) {
            if(step.getStepNumber() - openLoan.getStartStep()==12){
                sum =+ openLoan.getNumberLoanForAutoPlants() * 10000 + openLoan.getNumberLoanForBasicPlants() * 5000;
            }
            else {
                sum = +(openLoan.getNumberLoanForAutoPlants() * 10000 + openLoan.getNumberLoanForBasicPlants() * 5000) * 0.01;
            }
        }
        step.setCapital(step.getCapital() - (step.getNumberOfESM() * 300
                + step.getNumberOfEGP() * 500 + step.getNumberOfBasicPlants() * 1000
                + step.getNumberOfAutoPlants() * 1500 + (int) sum));
        gameStepRepository.save(step);
    }

    public Player getOldPlayer(GameStep step) {
        Collection<Player> players = step.getRoom().getPlayers();
        int numberOldPlayer = step.getStepNumber()%step.getRoom().getCurrentNumberOfPlayers()+1;
        for (Player player : players) {
            if (numberOldPlayer==player.getSeniorityNumber()) {
                return player;
            }
        }
        return null;
    }
    public void decisionBankByESM(Collection<GameStep> steps){
        ArrayList<Bid> bids = new ArrayList<>();
        for(GameStep step: steps){
            bids.add(step.getBid());
        }
        GameStep firstStep = ((ArrayList<GameStep>) steps).get(0);
        GameStep oldStep = gameStepRepository
                .getTopByPlayerOrderByGameStepIdDesc(getOldPlayer(firstStep));
        if(!bids.contains(oldStep.getBid())){
            throw new OutOfMemoryError();
        }
        int countESM = getCountESM(firstStep);
        for (int out = bids.size() - 1; out >= 1; out--) {  //Внешний цикл
            for (int in = 0; in < out; in++) {       //Внутренний цикл
                if (bids.get(in)!=null&&bids.get(in+1)!=null&&bids.get(in).getAmountOfMoney() < bids.get(in + 1).getAmountOfMoney()) {
                    Bid temp = bids.get(in);
                    bids.set(in, bids.get(in + 1));
                    bids.set(in + 1, temp);
                }
            }
        }
        for (int i = 0; i < bids.size(); i++){
            if(i<bids.size()-1
                    && bids.get(i)!=null&&bids.get(i+1)!=null&&bids.get(i).getAmountOfMoney().equals(bids.get(i + 1).getAmountOfMoney())
                    && bids.get(i + 1).getBidId().equals(oldStep.getBid().getBidId())){
            }
            else {
                if(bids.get(i)!=null) {
                    if (bids.get(i).getNumberOfUnits() <= countESM) {
                        bids.get(i).setBankDecision(bids.get(i).getNumberOfUnits());
                        countESM = countESM - bids.get(i).getNumberOfUnits();
                    } else {
                        bids.get(i).setBankDecision(countESM);
                        countESM = 0;
                    }
                }
            }
        }
        for(Bid bid: bids){
            if(bid!=null)
            bidDataService.saveBid(bid);
        }
        for(GameStep step: steps){
            if(step.getBid()!=null&&step.getBid().getBankDecision()!=null) {
                step.setCapital(step.getCapital() -
                        (step.getBid().getBankDecision() * step.getBid().getAmountOfMoney()));
                step.setNumberOfESM(step.getNumberOfESM() + step.getBid().getBankDecision());
                gameStepRepository.save(step);
            }
        }
    }
    public void decisionBankByEGP(Collection<GameStep> steps){
        ArrayList<Bid> bids = new ArrayList<>();
        for(GameStep step: steps){
            bids.add(step.getBid());
        }
        GameStep firstStep = ((ArrayList<GameStep>) steps).get(0);
        GameStep oldStep = gameStepRepository
                .getTopByPlayerOrderByGameStepIdDesc(getOldPlayer(firstStep));
        if(!bids.contains(oldStep.getBid())){
            throw new OutOfMemoryError();
        }
        int countEGP = getCountEGP(firstStep);
        for (int out = bids.size() - 1; out >= 1; out--) {  //Внешний цикл
            for (int in = 0; in < out; in++) {       //Внутренний цикл
                if (bids.get(in)!=null&&bids.get(in+1)!=null&&bids.get(in).getAmountOfMoney() > bids.get(in + 1).getAmountOfMoney()) {
                    Bid temp = bids.get(in);
                    bids.set(in, bids.get(in + 1));
                    bids.set(in + 1, temp);
                }
            }
        }
        for (int i = 0; i < bids.size(); i++){
            if(i<bids.size()-1
                    &&bids.get(i)!=null&&bids.get(i+1)!=null&&i<bids.size()-1
                    && bids.get(i).getAmountOfMoney().equals(bids.get(i + 1).getAmountOfMoney())
                    && bids.get(i + 1).getBidId().equals(oldStep.getBid().getBidId())){
            }
            else {
                if(bids.get(i)!=null && bids.get(i).getAmountOfMoney()>0) {
                    if (bids.get(i).getNumberOfUnits() <= countEGP) {
                        bids.get(i).setBankDecision(bids.get(i).getNumberOfUnits());
                        countEGP = countEGP - bids.get(i).getNumberOfUnits();
                    } else {
                        bids.get(i).setBankDecision(countEGP);
                        countEGP = 0;
                    }
                }
            }
        }
        for(Bid bid: bids){
            if(bid!=null) {
                bidDataService.saveBid(bid);
            }
        }
        for(GameStep step: steps){
            if(step.getBid()!=null) {
                step.setCapital(step.getCapital() +
                        (step.getBid().getBankDecision() * step.getBid().getAmountOfMoney()));
                step.setNumberOfEGP(step.getNumberOfEGP() - step.getBid().getBankDecision());
                gameStepRepository.save(step);
            }
        }
    }
    public int getCountESM(GameStep step) {
        int countPlayer = step.getRoom().getCurrentNumberOfPlayers();
        switch (step.getLevel()) {
            case 1:
                return countPlayer;
            case 2:
                return (int) (countPlayer * 1.5);
            case 3: return countPlayer*2;
            case 4: return (int)(countPlayer*2.5);
            case 5: return countPlayer*3;
            default: return 0;
        }
    }
    public int getMinMoneyESM(GameStep step){
        switch (step.getLevel()){
            case 1: return 800;
            case 2: return 650;
            case 3: return 500;
            case 4: return 400;
            case 5: return 300;
            default: return 0;
        }
    }
    public int getCountEGP(GameStep step){
        int countPlayer = step.getRoom().getCurrentNumberOfPlayers();
        switch (step.getLevel()){
            case 1: return countPlayer*3;
            case 2: return (int)(countPlayer*2.5);
            case 3: return countPlayer*2;
            case 4: return (int)(countPlayer*1.5);
            case 5: return countPlayer;
            default: return 0;
        }
    }
    public int getMaxMoneyEGP(GameStep step){
        switch (step.getLevel()){
            case 1: return 6500;
            case 2: return 6000;
            case 3: return 5500;
            case 4: return 5000;
            case 5: return 4500;
            default: return 0;
        }
    }
}
