package com.ssau.gruppa6414.repositories;

import com.ssau.gruppa6414.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface RoomRepository extends JpaRepository<Room,Integer> {
    @Query(value = "SELECT * FROM rooms where current_number_of_players < number_of_players ",
            nativeQuery = true)
    public Collection<Room> getAvailableRooms();
}
