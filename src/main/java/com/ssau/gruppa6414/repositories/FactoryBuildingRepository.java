package com.ssau.gruppa6414.repositories;

import com.ssau.gruppa6414.models.FactoryBuilding;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FactoryBuildingRepository extends JpaRepository<FactoryBuilding,Integer> {
}
