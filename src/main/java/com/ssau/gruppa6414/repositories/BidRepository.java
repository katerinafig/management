package com.ssau.gruppa6414.repositories;

import com.ssau.gruppa6414.models.Bid;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidRepository extends JpaRepository<Bid,Integer> {
}
