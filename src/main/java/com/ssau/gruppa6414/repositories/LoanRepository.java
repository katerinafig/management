package com.ssau.gruppa6414.repositories;

import com.ssau.gruppa6414.models.GameStep;
import com.ssau.gruppa6414.models.Loan;
import com.ssau.gruppa6414.models.Player;
import org.springframework.batch.core.Step;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;

public interface LoanRepository extends JpaRepository<Loan, Integer> {
    @Query(value = "SELECT * FROM loans where :stepNumber-start_step<13 and loans.player_id=:playerId",
            nativeQuery = true)
    Collection<Loan> getOpenedLoansByPlayerAndCurrentStep(@Param("stepNumber") Integer stepNumber,@Param("playerId") String playerId);



}
