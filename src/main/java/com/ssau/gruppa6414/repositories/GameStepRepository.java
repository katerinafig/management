package com.ssau.gruppa6414.repositories;

import com.ssau.gruppa6414.models.GameStep;
import com.ssau.gruppa6414.models.Player;
import com.ssau.gruppa6414.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface GameStepRepository extends JpaRepository<GameStep,Integer> {
    GameStep getTopByPlayerOrderByGameStepIdDesc (Player player);
    @Query(value = "select * from game_steps where game_steps.game_step_id in (select max(game_step_id) as game_step_id from game_steps where room_id = :roomId group by player_id)",
            nativeQuery = true )
    Collection<GameStep> getLastStepsFromRoom(@Param("roomId") Integer roomId);
    @Query(value = "select * from game_steps where room_id=:roomId order by game_step_id desc limit 1",
            nativeQuery = true )
    GameStep getLastGameStep(@Param("roomId") Integer roomId);
    Collection<GameStep> getGameStepsByRoomAndStageNumberAndStepNumber(
            Room room, Integer stageNumber,Integer stepNumber);
    Collection<GameStep> getGameStepsByPlayer(Player player);
}
