package com.ssau.gruppa6414.repositories;

import com.ssau.gruppa6414.models.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player,String> {
}
