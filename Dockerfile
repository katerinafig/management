FROM openjdk:11-jdk
COPY /target/management-1.0.jar management-1.0.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","management-1.0.jar"]